# Copyright (c) 2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//arkcompiler/runtime_core/ark_config.gni")
import("//arkcompiler/runtime_core/libabckit/libabckit_config.gni")
declare_args() {
  enable_libabckit_coverage = false
}

config("libabckit_coverage") {
  if (enable_libabckit_coverage) {
    cflags = [
      "-fprofile-instr-generate",
      "-fcoverage-mapping",
      "-mllvm",
      "-runtime-counter-relocation",
    ]

    ldflags = [
      "-fprofile-instr-generate",
      "-fcoverage-mapping",
    ]
  }
}

config("libabckit_config") {
  cflags_cc = [
    "-std=c++17",
    "-pedantic",
    "-pedantic-errors",
    "-Wall",
    "-Wextra",
    "-Werror",
  ]

  if (!is_debug) {
    defines = [ "NDEBUG" ]
  }

  if (libabckit_with_sanitizers) {
    cflags = [
      "-fsanitize=address",
      "-fsanitize-address-use-after-scope",
      "-fsanitize=undefined",
      "-fno-omit-frame-pointer",
    ]

    ldflags = [
      "-lasan",
      "-lubsan",
    ]
  }

  configs = [ sdk_libc_secshared_config ]
}

ark_gen_file("abckit_options_gen_h") {
  template_file = "$ark_root/templates/options/options.h.erb"
  data_file = "$ark_root/libabckit/src/options.yaml"
  requires = [ "$ark_root/templates/common.rb" ]
  output_file = "$target_gen_dir/generated/tmp/abckit_options_gen.h"
}

action("abckit_options") {
  script = "./scripts/fix_options.sh"
  args = [
    rebase_path("$target_gen_dir/generated/tmp/abckit_options_gen.h"),
    rebase_path("$target_gen_dir/generated/abckit_options_gen.h"),
  ]
  inputs = [ "$ark_root/libabckit/src/options.yaml" ]
  outputs = [ "$target_gen_dir/generated/abckit_options_gen.h" ]
  deps = [ ":abckit_options_gen_h" ]
}

action("get_abckit_status") {
  script = "scripts/get_abckit_status.py"
  outputs = [ "$target_gen_dir/abckit_status.csv" ]
}

ohos_executable("abckit_canary") {
  cflags = [
    "-std=c99",
    "-pedantic",
    "-pedantic-errors",
    "-Wall",
    "-Wextra",
    "-Werror",
    "-Wno-typedef-redefinition",
  ]
  cflags += [ "-Wno-typedef-redefinition" ]  # NOTE(ivagin)
  sources = [ "tests/canary.c" ]
  include_dirs = [ "$ark_root" ]
  install_enable = false
  part_name = "runtime_core"
  subsystem_name = "arkcompiler"
}

group("libabckit_packages") {
  if (enable_libabckit) {
    deps = [
      ":libabckit(${host_toolchain})",
      "abckit:abckit(${host_toolchain})",
    ]
  }
}

group("libabckit_tests") {
  testonly = true
  deps = [
    ":abckit_canary(${host_toolchain})",
    ":abckit_check_clang_format",
    ":abckit_check_clang_tidy",
    ":abckit_documentation",
    ":get_abckit_status",
    "tests:AbcKitTestAction",
  ]
}

config("libabckit_public_config") {
  include_dirs = [ "include" ]
}

ohos_source_set("libabckit_static") {
  sources = [
    "src/abckit_impl.cpp",
    "src/helpers_common.cpp",
    "src/ir_impl.cpp",
    "src/ir_interface_impl.cpp",
    "src/isa_dynamic_impl.cpp",
    "src/isa_dynamic_impl_instr_1.cpp",
    "src/isa_dynamic_impl_instr_2.cpp",
    "src/isa_static_impl.cpp",
    "src/metadata_arkts_inspect_impl.cpp",
    "src/metadata_arkts_modify_impl.cpp",
    "src/metadata_inspect_impl.cpp",
    "src/metadata_js_inspect_impl.cpp",
    "src/metadata_js_modify_impl.cpp",
    "src/metadata_modify_impl.cpp",
    "src/statuses_impl.cpp",
  ]

  include_dirs = [
    "$ark_root",
    "$ark_root/libpandabase",
    "$target_gen_dir/src",
  ]

  configs = [
    "$abckit_root:libabckit_coverage",
    ":libabckit_config",
  ]

  deps = [ ":abckit_options" ]

  part_name = "runtime_core"
  subsystem_name = "arkcompiler"
}

group("libabckit_adapter_static_header_deps") {
  deps = [
    "src/adapter_dynamic/templates:isa_gen_libabckit_inst_props_helpers_dynamic_inc",
    "src/adapter_static:get_intrinsic_id_static_inc",
    "src/templates/abckit_intrinsics:ark_gen_libabckit_abckit_intrinsics_inl",
    "src/templates/abckit_intrinsics:ark_gen_libabckit_abckit_intrinsics_opcodes_inc",
    "src/templates/abckit_intrinsics:ark_gen_libabckit_abckit_intrinsics_vreg_width_h",
    "src/templates/dyn_intrinsics:isa_gen_libabckit_dyn_intrinsics_enum_inc",
    "src/templates/dyn_intrinsics:isa_gen_libabckit_dyn_intrinsics_flags_inc",
    "src/templates/dyn_intrinsics:isa_gen_libabckit_get_dyn_intrinsics_names_inc",
  ]
}

ohos_source_set("libabckit_adapter_static_source_set") {
  sources = [
    "src/adapter_static/abckit_static.cpp",
    "src/adapter_static/helpers_static.cpp",
    "src/adapter_static/ir_static.cpp",
    "src/adapter_static/ir_static_instr_1.cpp",
    "src/adapter_static/metadata_inspect_static.cpp",
    "src/adapter_static/metadata_modify_static.cpp",
    "src/adapter_static/runtime_adapter_static.cpp",
  ]

  ins_create_wrapper_dyn_dir = get_label_info(
          "$ark_root/libabckit/src/wrappers:isa_gen_libabckit_ins_create_wrapper_api_inc",
          "target_gen_dir")
  intrinsics_gen_dir = get_label_info(
          "$ark_root/libabckit/src/adapter_dynamic/templates:isa_gen_libabckit_dyn_intrinsics_enum_inl",
          "target_gen_dir")
  include_dirs = [
    "$ark_root/static_core",  # this target should not include headers from
                              # dynamic runtime, so static_core must be listed
                              # first
    "$ark_root",
    "$ark_root/libabckit",
    "$ark_root/include",
    "$target_gen_dir/src/codegen",
    "$target_gen_dir/src/adapter_static",
    "$target_gen_dir/src/adapter_dynamic/generated",
    "$target_gen_dir/libabckit/src",
    "$intrinsics_gen_dir/../generated",
    "$ins_create_wrapper_dyn_dir",
  ]

  part_name = "runtime_core"
  subsystem_name = "arkcompiler"

  configs = [
    "$ark_root/static_core/assembler:arkassembler_public_config",
    "$ark_root/static_core/bytecode_optimizer:bytecodeopt_public_config",
    "$ark_root/static_core/compiler:arkcompiler_public_config",
    "$ark_root/static_core/libpandafile:arkfile_public_config",
    "$ark_root/static_core/libpandabase:arkbase_public_config",
    "$ark_root/static_core/runtime:arkruntime_public_config",  # NOTE
    "$ark_root/static_core:ark_config",  # NOTE
    ":libabckit_config",
  ]

  configs += [ "$abckit_root:libabckit_coverage" ]

  deps = [
    ":concat_abckit_intrinsics_yaml",
    ":libabckit_adapter_static_header_deps",
    "$ark_root/static_core/abc2program:arkts_abc2program",
    "$ark_root/static_core/assembler:libarktsassembler",
    "$ark_root/static_core/assembler:libarktsassembler",
    "$ark_root/static_core/bytecode_optimizer:libarktsbytecodeopt",
    "$ark_root/static_core/bytecode_optimizer:libarktsbytecodeopt",
    "$ark_root/static_core/libpandabase:libarktsbase",
    "$ark_root/static_core/libpandafile:libarktsfile",
  ]
}

abckit_yaml = "abckit_compiler_intrinsics.yaml"

action("crop_abckit_intrinsics_yaml") {
  script = "./scripts/fix_intrinsics_yml.sh"
  args = [
    rebase_path("src/$abckit_yaml"),
    rebase_path("$target_gen_dir/generated/$abckit_yaml"),
  ]
  inputs = [ "src/$abckit_yaml" ]
  outputs = [ "$target_gen_dir/generated/$abckit_yaml" ]
}

runtime_intrinsics_gen_dir =
    get_label_info(
        "$ark_root/static_core/runtime:arkruntime_gen_intrinsics_yaml",
        "target_gen_dir")
concat_yamls("concat_abckit_intrinsics_yaml") {
  extra_dependencies = [
    "$ark_root/static_core/runtime:arkruntime_gen_intrinsics_yaml",
    ":crop_abckit_intrinsics_yaml",
  ]
  output_file = "$runtime_intrinsics_gen_dir/abckit_intrinsics.yaml"
  default_file = "$runtime_intrinsics_gen_dir/intrinsics.yaml"
  add_yamls = [ "$target_gen_dir/generated/$abckit_yaml" ]
}

group("libabckit_adapter_dynamic_header_deps") {
  deps = [
    "src/templates/abckit_intrinsics:ark_gen_libabckit_abckit_intrinsics_inl",
    "src/templates/abckit_intrinsics:ark_gen_libabckit_abckit_intrinsics_opcodes_inc",
    "src/templates/abckit_intrinsics:ark_gen_libabckit_abckit_intrinsics_vreg_width_h",
    "src/templates/dyn_intrinsics:isa_gen_libabckit_dyn_intrinsics_enum_inc",
    "src/templates/dyn_intrinsics:isa_gen_libabckit_dyn_intrinsics_flags_inc",
    "src/templates/dyn_intrinsics:isa_gen_libabckit_get_dyn_intrinsics_names_inc",
  ]
}

ohos_source_set("libabckit_adapter_dynamic_source_set") {
  sources = [
    "src/adapter_dynamic/abckit_dynamic.cpp",
    "src/adapter_dynamic/helpers_dynamic.cpp",
    "src/adapter_dynamic/metadata_inspect_dynamic.cpp",
    "src/adapter_dynamic/metadata_modify_dynamic.cpp",
  ]

  ins_create_wrapper_dyn_dir = get_label_info(
          "$ark_root/libabckit/src/wrappers:isa_gen_libabckit_ins_create_wrapper_api_inc",
          "target_gen_dir")
  include_dirs = [
    "$ark_root",
    "$ark_root/libabckit",
    "$ark_root/include",
    "$ins_create_wrapper_dyn_dir",
  ]

  part_name = "runtime_core"
  subsystem_name = "arkcompiler"

  configs = [
    "$ark_root:ark_config",
    "$ark_root/assembler:arkassembler_public_config",
    "$ark_root/libpandabase:arkbase_public_config",
    "$ark_root/libpandafile:arkfile_public_config",
    ":libabckit_config",
  ]

  configs += [ "$abckit_root:libabckit_coverage" ]

  deps = [
    ":concat_abckit_intrinsics_yaml",
    ":libabckit_adapter_dynamic_header_deps",
    "$ark_root/abc2program:abc2program",
    "$ark_root/assembler:libarkassembler",
    "src/wrappers/graph_wrapper:libabckit_graph_wrapper_source_set",
  ]

  # external_deps = [
  #   "ets_runtime:libark_jsruntime",
  # ]
}

ohos_shared_library("libabckit") {
  deps = [
    ":libabckit_adapter_dynamic_source_set",
    ":libabckit_adapter_static_source_set",
    ":libabckit_static",
    "src/codegen:libabckit_codegen_dynamic_source_set",
    "src/codegen:libabckit_codegen_static_source_set",
    "src/irbuilder_dynamic:libabckit_ir_builder_dynamic_source_set",
    "src/mem_manager:libabckit_mem_manager_source_set",
    "src/wrappers:libabckit_abcfile_wrapper_source_set",
    "src/wrappers:libabckit_pandasm_wrapper_source_set",
  ]

  configs = [
    "$abckit_root:libabckit_coverage",
    ":libabckit_config",
  ]

  if (is_linux) {
    libs = [ "stdc++fs" ]
  }

  output_extension = "so"
  part_name = "runtime_core"
  subsystem_name = "arkcompiler"
}

action("abckit_documentation") {
  script = "$ark_root/libabckit/scripts/run_script.sh"

  args = [
    "--script=doxygen",
    "--ret-code=0",
    "--env=ABCKIT_DOXYGEN_OUT_DIR=" + rebase_path("$target_gen_dir") +
        "/doxygen/",
    "--env=ABCKIT_PROJECT_ROOT=" + rebase_path("$ark_root") + "/libabckit/",
    "--script-args=" + rebase_path("$ark_root") + "/libabckit/.doxygen",
  ]

  outputs = [ "$target_gen_dir/abckit_documentation_status.txt" ]
}

action("abckit_force_clang_format") {
  script = "$ark_root/static_core/scripts/code_style/code_style_check.py"
  args = [
    rebase_path("$ark_root") + "/libabckit/",
    "--reformat",
  ]
  outputs = [ "$target_gen_dir/generated/tmp/force_clang_format_status.txt" ]
}

action("abckit_check_clang_format") {
  script = "$ark_root/static_core/scripts/code_style/code_style_check.py"
  args = [ rebase_path("$ark_root") + "/libabckit/" ]
  outputs = [ "$target_gen_dir/generated/tmp/check_clang_format_status.txt" ]
}

action("abckit_check_clang_tidy") {
  script = "$ark_root/static_core/scripts/clang-tidy/clang_tidy_check.py"
  args = [
    rebase_path("$ark_root"),
    rebase_path("$root_out_dir"),
    "--check-libabckit",
    "--filename-filter=libabckit/*",
    "--header-filter=.*/libabckit/.*",
  ]
  outputs = [ "$target_gen_dir/generated/tmp/check_clang_tidy_status.txt" ]

  deps = [
    ":abckit_options",
    ":libabckit",
  ]
}
