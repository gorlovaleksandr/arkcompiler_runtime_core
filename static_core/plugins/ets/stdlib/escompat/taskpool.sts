/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package escompat;

export type taskpoolCallbackFunction = () => void;

export type taskpoolCallbackFunctionWithError = (e: Error) => void;

// NOTE(ipetrov, #16281): Make taskpool.Task when namespaces will be supported
/**
 * @class Task provides an interface to create a task to execute in the taskpool
 */
export class taskpoolTask {
    // NOTE(ipetrov, #17246, #17339): support arguments after es2apnda support spread expressions and common function super-type
    /**
     * Create a Task instance
     *
     * @param name The name of task
     * @param func Concurrent function to execute in the taskpool
     */
    constructor(name: string, func: Function0<NullishType>) {
        this.name = name;
        this.func = func;
        this.args = [];
        // NOTE(ipetrov, #15499): support information about durations
        this.totalDuration = 0.0;
        this.ioDuration = 0.0;
        this.cpuDuration = 0.0;
        // Each task has unique identifier
        this.id = taskpoolTask.generateId();
    }

    /**
     * Create a Task instance
     *
     * @param func Concurrent function to execute in the taskpool
     */
    constructor(func: Function0<NullishType>) {
        this("", func);
    }

    /**
     * Check current running task is canceled or not
     *
     * @returns true if current running task is canceled, false - otherwise
     */
    static native isCancel(): boolean;

    /**
     * Register a callback and call it when the task is enqueued
     * 
     * @param callback Callback to be registered and executed when the task is enqueued
     * @throws Error if task is executed. It does not support the registration of listeners
     */
    onEnqueued(callback: taskpoolCallbackFunction): void {
        this.throwIfCallbackCannotBeAdded();
        this.onEnqueueCallback = callback;
    }

    /**
     * Register a callback and call it before the task execution
     * 
     * @param callback Callback to be registered and executed before the task execution
     * @throws Error if task is executed. It does not support the registration of listeners
     */
    onStartExecution(callback: taskpoolCallbackFunction): void {
        this.throwIfCallbackCannotBeAdded();
        this.onStartCallback = callback;
    }

    /**
     * Register a callback and call it when the task fails to execute
     * 
     * @param callback Callback to be registered and executed when the task fails to execute
     * @throws Error if task is executed. It does not support the registration of listeners
     */
    onExecutionFailed(callback: taskpoolCallbackFunctionWithError): void {
        this.throwIfCallbackCannotBeAdded();
        this.onFailCallback = callback;
    }

    /**
     * Register a callback and call it when the task successfully executed
     * 
     * @param callback Callback to be registered and executed when the task successfully executed
     * @throws Error if task is executed. It does not support the registration of listeners
     */
    onExecutionSucceeded(callback: taskpoolCallbackFunction): void {
        this.throwIfCallbackCannotBeAdded();
        this.onSuccessCallback = callback;
    }

    /**
     * @returns true if the task has been completed, false - otherwise
     */
    isDone(): boolean {
        // NOTE(ipetrov, #15499): Need to support, potentially need to refactor native implementation
        throw new Error("Does not support");
    }

    internal checkExecution(): void {
        if (this.groupId != 0) {
            throw new Error("taskpool:: groupTask cannot execute outside");
        }
        if (this.seqId != 0) {
            throw new Error("taskpool:: seqRunnerTask cannot execute outside");
        }
    }

    internal execute(): NullishType {
        if (!taskpool.taskStarted(this.id, this.groupId)) {
            this.throwCancelTaskError();
        }
        this.callUserCallback(this.onStartCallback);
        let result: NullishType = null;
        try {
            result = this.func();
        } catch (e: Error) {
            try {
                this.onFailCallback?.(e);
            } catch (callbackError) {}
            throw e;
        } finally {
            if (!taskpool.taskFinished(this.id, this.groupId)) {
                this.throwCancelTaskError();
            }
        }
        this.callUserCallback(this.onSuccessCallback);
        return result;
    }

    private throwCancelTaskError(): void {
        if (this.groupId != 0) {
            throw new Error("taskpool:: taskGroup has been canceled");
        }
        if (this.seqId != 0) {
            throw new Error("taskpool:: sequenceRunner task has been canceled");
        }
        throw new Error("taskpool:: task has been canceled");
    }

    private throwIfCallbackCannotBeAdded() {
        if (this.isSubmitted) {
            throw new Error("taskpool:: The executed task does not support the registration of listeners.");
        }
    }

    private callUserCallback(callback: taskpoolCallbackFunction | undefined): void {
        try {
            callback?.();
        } catch (e) {}
    }

    internal enqueue(): void {
        this.isSubmitted = true;
        this.callUserCallback(this.onEnqueueCallback);
    }

    private static native generateId(): long;

    totalDuration: number;
    ioDuration: number;
    cpuDuration: number;
    func: Function0<NullishType>;
    args: NullishType[];
    name: string;

    internal readonly id: long;
    internal groupId: long = 0;
    internal seqId: long = 0;
    internal isSubmitted: boolean = false;

    private onEnqueueCallback?: taskpoolCallbackFunction;
    private onStartCallback?: taskpoolCallbackFunction;
    private onFailCallback?: taskpoolCallbackFunctionWithError;
    private onSuccessCallback?: taskpoolCallbackFunction;
}

// NOTE(ipetrov, #16281): Make taskpool.LongTask when namespaces will be supported
/**
 * @class LongTask provides an interface to create a long executing task.
 * The such task can be executed only once
 * @extends Task 
 */
export class taskpoolLongTask extends taskpoolTask {
    constructor(name: string, func: Function0<NullishType>) {
        super(name, func);
    }
    constructor(func: Function0<NullishType>) {
        super("", func);
    }

    internal override checkExecution(): void {
        super.checkExecution();
        if (this.isSubmitted) {
            throw new Error("taskpool:: The long task can only be executed once");
        }
    }
}

// NOTE(ipetrov, #16281): Make taskpool.TaskGroup when namespaces will be supported
/**
 * @class TaskGroup provides an interface to create a task group
 */
export class taskpoolTaskGroup {
    /**
     * Create a TaskGroup instance
     */
    constructor() {
        this("");
    }

    /**
     * Create a TaskGroup instance
     * @param name The name of TaskGroup
     */
    constructor(name: string) {
        this.name = name;
        this.id = taskpoolTaskGroup.generateGroupId();
        this.tasks = new Array<taskpoolTask>();
    }

    /**
     * Add a concurrent function into task group
     *
     * @param func the concurrent function to add to the task group
     */
    addTask(func: Function0<NullishType>): void {
        this.addTask(new taskpoolTask(func));
    }

    /**
     * Add a task into task group
     *
     * @param task the task to add to the task group
     * @throws Error if
     *     - task was already added to any group
     *     - task is LongTask
     *     - task was executed outside the group
     *     - task executed via SequenceRunner
     */
    addTask(task: taskpoolTask): void {
        if (task.groupId != 0) {
            throw new Error("taskpool:: taskGroup cannot add groupTask");
        } else if (task instanceof taskpoolLongTask) {
            throw new Error("taskpool:: The interface does not support the long task");
        } else if (task.isSubmitted || task.seqId != 0) {
            throw new Error("taskpool:: taskGroup cannot add seqRunnerTask or executedTask");
        }
        task.groupId = this.id;
        this.tasks.push(task);
    }

    /**
     * Generate unique identifier for the group
     * @see TaskGroup.constructor
     * @return unique group identifier for a new Taskgroup instance 
     */
    private static native generateGroupId(): long;

    /**
     * TaskGroup name
     */
    name: string;

    internal readonly id: long;
    internal tasks: Array<taskpoolTask>;
    internal isSubmitted: boolean = false;
}

// NOTE(ipetrov, #16281): Temporary solution, make as namespace when namespaces will be supported
/**
 * @class SequenceRunner provides a queue, in which all tasks are executed in sequence
 */
export class taskpoolSequenceRunner {
    /**
     * Create a SequenceRunner instance
     */
    constructor(/* priority?: Priority */) {
        this.id = taskpoolSequenceRunner.generateSeqRunnerId();
        this.seqPromise = Promise.resolve<NullishType>(new Object());
    }

    execute(task: taskpoolTask): Promise<NullishType> {
        this.checkExecution(task);
        task.seqId = this.id;
        taskpool.taskSubmitted(task.id);
        task.enqueue();
        let taskRunner = (value: NullishType): NullishType => {
            return task.execute();
        };
        this.seqPromise = this.seqPromise.then<NullishType, NullishType>(taskRunner, taskRunner);
        return this.seqPromise;
    }

    private static native generateSeqRunnerId(): long;

    private checkExecution(task: taskpoolTask) {
        if (task.groupId != 0) {
            throw new Error("taskpool:: SequenceRunner cannot execute groupTask");
        }
        if (task.isSubmitted || task.seqId != 0) {
            throw new Error("taskpool:: SequenceRunner cannot execute seqRunnerTask or executedTask");
        }
    }

    internal readonly id: long;
    private seqPromise: Promise<NullishType>;
}

// NOTE(ipetrov, #16281): Temporary solution, make as namespace when namespaces will be supported
export class taskpool {
    static execute(func: Function0<NullishType>): Promise<NullishType> {
        return launch func();
    }
    static execute<A1>(func: Function1<A1, NullishType>, a1: A1): Promise<NullishType> {
        return launch func(a1);
    }
    static execute<A1, A2>(func: Function2<A1, A2, NullishType>, a1: A1, a2: A2): Promise<NullishType> {
        return launch func(a1, a2);
    }
    static execute<A1, A2, A3>(func: Function3<A1, A2, A3, NullishType>, a1: A1, a2: A2, a3: A3): Promise<NullishType> {
        return launch func(a1, a2, a3);
    }
    static execute<A1, A2, A3, A4>(func: Function4<A1, A2, A3, A4, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4): Promise<NullishType> {
        return launch func(a1, a2, a3, a4);
    }
    static execute<A1, A2, A3, A4, A5>(func: Function5<A1, A2, A3, A4, A5, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5);
    }
    static execute<A1, A2, A3, A4, A5, A6>(func: Function6<A1, A2, A3, A4, A5, A6, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7>(func: Function7<A1, A2, A3, A4, A5, A6, A7, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8>(func: Function8<A1, A2, A3, A4, A5, A6, A7, A8, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9>(func: Function9<A1, A2, A3, A4, A5, A6, A7, A8, A9, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10>(func: Function10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11>(func: Function11<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10, a11: A11): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12>(func: Function12<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10, a11: A11, a12: A12): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13>(func: Function13<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10, a11: A11, a12: A12, a13: A13): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14>(func: Function14<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10, a11: A11, a12: A12, a13: A13, a14: A14): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15>(func: Function15<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10, a11: A11, a12: A12, a13: A13, a14: A14, a15: A15): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15);
    }
    static execute<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16>(func: Function16<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, NullishType>, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10, a11: A11, a12: A12, a13: A13, a14: A14, a15: A15, a16: A16): Promise<NullishType> {
        return launch func(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16);
    }

    /**
     * Execute a concurrent task
     * 
     * @param task The task for executing
     * @returns Promise for result of executed task
     */
    static execute(task: taskpoolTask /*, priority?: Priority */): Promise<NullishType> {
        task.checkExecution();
        taskpool.taskSubmitted(task.id);
        task.enqueue();
        return launch task.execute();
    }

    /**
     * Execute a concurrent task group
     *
     * @param group The task group for execution
     * @returns Promise for array of results of executed tasks from the group
     */
    static execute(group: taskpoolTaskGroup /*, priority?: Priority */): Promise<Array<NullishType>> {
        let tasksCount: long = group.tasks.length as long;
        if (tasksCount == 0) {
            group.isSubmitted = true;
            return Promise.resolve(new Array<NullishType>());
        }
        taskpool.taskGroupSubmitted(group.id, tasksCount);
        let promises = new Array<Promise<NullishType>>();
        group.tasks.forEach((task: taskpoolTask, i: number) => {
            task.enqueue();
            promises.push(launch task.execute());
        });
        group.isSubmitted = true;
        return Promise.all<NullishType>(promises);
    }

    /**
     * Cancel a concurrent task
     *
     * @param task The task for canceling
     * @throws Error if the task does not exist when it is canceled
     * @see Task.isCancel
     */
    static cancel(task: taskpoolTask): void {
        taskpool.cancelImpl(task.id, task.seqId);
    }

    /**
     * Cancel a concurrent task group
     *
     * @param group The group for canceling
     * @throws Error if the task group does not exist when it is canceled
     */
    static cancel(group: taskpoolTaskGroup): void {
        taskpool.cancelGroupImpl(group.id);
    }

    /**
     * Terminate a long task.
     *
     * @param longTask The long task for terminating
     * @note This method is needed for compatibilty with OHOS API. For coroutines in static ArkTS it is not required 
     */
    static terminateTask(longTask: taskpoolLongTask): void {}

    private static native cancelImpl(taskId: long, seqId: long): void;

    private static native cancelGroupImpl(groupId: long): void;

    /**
     * @brief Submit task to the taskpool on the execute method
     * @see taskpool.execute
     * @see taskpool.SequenceRunner.execute
     * @param taskId identifier of the submitted task
     */
    internal static native taskSubmitted(taskId: long): void;

    /**
     * @brief Submit group of tasks to the taskpool on the execute method
     * @see taskpool.execute
     * @param groupId identifier of the submitted group
     * @param tasksCount count of tasks in the passed group 
     */
    private static native taskGroupSubmitted(groupId: long, tasksCount: long): void;

    /**
     * @brief Notify the taskpool that the task is started on a coroutine
     * @param taskId identifier of the started task
     * @param groupId group identifier of the passed task (0 means task is not group task)
     * @returns true if task is not cancled by cancel method, false - otherwise
     *
     * @see taskpool.Task.execute
     * @see taskpool.cancel
     */
    internal static native taskStarted(taskId: long, groupId: long): boolean;

    /**
     * @brief Notify the taskpool that the task is finished on a coroutine
     * @param taskId identifier of the finished task
     * @param groupId group identifier of the passed task (0 means task is not group task)
     * @returns true if task is not cancled by cancel method, false - otherwise
     *
     * @see taskpool.Task.execute
     * @see taskpool.cancel
     */
    internal static native taskFinished(taskId: long, groupId: long): boolean;
}
